const mongoose = require('mongoose');

const model = mongoose.model('Analytics', {
    name: {
        type: String,
        required: true
    },
    hits: {
        type: Number,
        required: true
    }
});
