const mongoose = require('mongoose');

const model = mongoose.model('UserRegistration', {
    email: {
        type: String,
        required: true,
        unique: true
    },
    code: {
        type: String,
        required: true
    }
});
