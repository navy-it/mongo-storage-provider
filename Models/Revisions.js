const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const revisionSchema = new Schema({
    date: {
        type: Date,
        default: (Date.now())
    },
    user: {
        type: Schema.Types.ObjectId,
    },
    revisionDocument: {
        type: Schema.Types.ObjectId,
    },
    description: {
        type: String,
    },
    type: {
        type: String,
        default: "modified"
    }
});

const model = mongoose.model('Revisions', {
    title: {
        type: String,
        required: true
    },
    fileName: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    tags: {
        type: Array,
        default: []
    },
    creationDate: {
        type: Date,
        required: true,
        default: (Date.now())
    },
    updateDate: {
        type: Date,
        required: true,
        default: (Date.now())
    },
    user: {
        type: String,
        required: true
    },
    type: {
        type: String,
        default: "modified"
    },
    revisions: {
        type: Array,
        default: []
    }
});