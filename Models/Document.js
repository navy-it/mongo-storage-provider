const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const revisionSchema = new Schema({
    date: {
        type: Date,
        required: true,
        default: (Date.now())
    },
    user: {
        type: Schema.Types.ObjectId,
        required: true
    },
    revisionDocument: {
        type: Schema.Types.ObjectId,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    type: {
        type: String,
        default: "modified"
    }
});

const model = mongoose.model('Document', {
    title: {
        type: String,
        required: true
    },
    fileName: {
        type: String,
        required: true,
        unique: true
    },
    content: {
        type: String,
        required: true
    },
    tags: {
        type: Array,
        default: []
    },
    creationDate: {
        type: Date,
        required: true,
        default: (Date.now())
    },
    updateDate: {
        type: Date,
        required: true,
        default: (Date.now())
    },
    revisions: {
        type: Array,
        default: []
    }
});
