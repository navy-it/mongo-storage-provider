"use strict";

const StorageProvider = require('storage-provider');
const mongoose = require('mongoose');
const User = require('./Models/User');
const Analytics = require('./Models/Analytics');
const Document = require('./Models/Document');
const UserRegistration = require('./Models/UserRegistration');
const Revisions = require('./Models/Revisions');

class MongoStorageProvider extends StorageProvider {
    constructor(config, parserUtility) {
        super(config, parserUtility);
        this.connectDatabase();
    }

    connectDatabase() {
        if (!this._config.database_address) {
            console.err("You must specify `database_address` in the default.config.js file.");
            return;
        }

        mongoose.connect(this._config.database_address);
    }

    readDocument(slug, callback) {
        if (!slug) return null;

        slug = slug.toLowerCase();

        mongoose.model('Document').findOne({fileName: slug + ".md"}, function(err, doc) {
            if (err) {
                return callback(err);
            }

            if (!doc) {
                return callback(null, null);
            }

            let document = this._parser.getDocumentFromFileContent(doc.content);
            document.slug = slug;
            if (!document.title) {
                document.title = this.slugToTitle(slug);
            }
            document.revisions = doc.revisions;
            var users_find = [];
            for (var i = 0; i < document.revisions.length; i++) {
                users_find.push(document.revisions[i].user);
            }


            if (document.revisions.length === 0) {
                return callback(null, document);
            }

            this.queryFindUser({_id: {$in: users_find}}, (err, users) => {
                if (err) {
                    return callback(err);
                }

                for (var i = 0; i < document.revisions.length; i++) {
                    let rev = document.revisions[i];

                    for (var u = 0; u < users.length; u++) {
                        let user = users[u];
                        if (rev.user === user._id.toString()) {
                            document.revisions[i].user = user.email;
                        }
                    }
                }

                return callback(null, document);
            });
        }.bind(this));
    }

    /*
     * Updates the document
     * 
     *
     *
     *
     */
    storeDocument(document, callback, type) {
        type = type || "modified";

        document.slug = document.slug.toLowerCase();
        document.fileName = document.slug + ".md";
        document.content = this._parser.convertDocumentToFileContent(document);


        var model = mongoose.model("Document");
        var revisionUser = document.revisionInfo.user;
        delete document.revisionInfo;
        //Just in case so the revision history doesn't get lost
        if (document.revisions) delete document.revisions;

        model.findOneAndUpdate({fileName: document.fileName}, document, {
            upsert: true,
            new: true //So that the newly inserted document gets returned
        }, (err, previousObject) => {
            if (err) {
                return callback(err);
            }

            this.createRevision(previousObject, type, revisionUser, (err) => {
                if (err) {
                    return callback(err);
                }

                return callback(null, previousObject);
            });
        });
    }

    /*
     * Stores document in the revisions collection
     * and puts revision info on the current document
     */
    createRevision(document, type, user, callback) {
        var model = mongoose.model("Document");

        var updatedDoc = {
            title: document.title,
            content: document.content,
            tags: document.tags,
            user: user,
            fileName: document.fileName,
            revisions: document.revisions,
            type: type
        };
        //Going to insert the document into another collection
        var revisionModel = mongoose.model("Revisions");
        var obj = new revisionModel(updatedDoc);

        obj.save((err, revisedDocument) => {
            if (err) {
                return callback(err);
            }

            var revisionInfo = {
                user: user,
                date: Date.now(),
                revisionDocument: revisedDocument._id,
                type: type
            };

            mongoose.model("Document").findOneAndUpdate({fileName: document.fileName}, {$push: {revisions: revisionInfo}},
                (err) => {
                    if (err) {
                        return callback(err);
                    }

                    return callback(null);
                });
        });
    }

    restoreRevision(slug, revisionId, userId, callback) {
        this.queryFindRevisions(slug, revisionId, (err, document) => {
            if (err) {
                return callback(err);
            }

            document.revisionInfo = {
                user: userId
            };

            this.storeDocument(document, (err, storedDoc) => {
                if (err) {
                    return callback(err);
                }

                return callback();
            }, 'restored');
        });
    }

    queryFindDocuments(query, callback) {
        mongoose.model("Document").find(query, null, (err, documents) => {
            if (err) {
                return callback(err);
            }

            for (var i = 0; i < documents.length; i++) {
                documents[i].slug = documents[i].fileName.replace('.md', '');
            }

            return callback(null, documents);
        });
    }

    queryFindRevisions(slug, revisionId, callback) {
        if (!slug) return null;

        slug = slug.toLowerCase();

        mongoose.model('Revisions').findOne({_id: revisionId}, function(err, doc) {
            if (err) {
                return callback(err);
            }

            if (!doc) {
                return callback(null, null);
            }

            let document = this._parser.getDocumentFromFileContent(doc.content);
            document.slug = slug;
            if (!document.title) {
                document.title = this.slugToTitle(slug);
            }
            document.revisions = doc.revisions;
            var users = [];
            for (var i = 0; i < document.revisions.length; i++) {
                users.push(document.revisions[i].user);
            }

            this.queryFindUser({_id: {$in: users}}, (err, users) => {
                if (err) {
                    return callback(err);
                }

                for (var i = 0; i < document.revisions.length; i++) {
                    let rev = document.revisions[i];

                    for (var u = 0; u < users.length; u++) {
                        let user = users[u];
                        if (rev.user === user._id.toString()) {
                            document.revisions[i].user = user.email;
                        }
                    }
                }

                return callback(null, document);
            });
        }.bind(this));
    }

    hasDocument(slug, callback) {
        slug = slug.toLowerCase();

        mongoose.model("Document").find({fileName: slug + ".md"}, 'filename', (err, docs) => {
            if (err) {
                return callback(err);
            }

            return callback(null, docs.length > 0);
        });
    }

    deleteDocument(document, callback) {
        document.slug = document.slug.toLowerCase();
        let filePath = document.slug + ".md";

        mongoose.model("Document").deleteOne({fileName: filePath}, function(err) {
            return callback(err);
        });
    }

    getAllDocuments(callback) {
        mongoose.model("Document").find({}, null, function(err, docs) {
            if (err) {
                return callback(err);
            }

            return callback(null, docs);
        }.bind(this));
    }

    queryFindUser(query, callback) {
        return this.readObject("User", query, callback);
    }

    /*
     * Supported "objType"
     * User -- creates a user object
     * Analytics -- creates an Analytics object
     */
    storeObject(objType, object, callback) {
        var model = mongoose.model(objType);
        var obj = new model(object);

        obj.save(function (err, savedObject) {
            return callback(err, savedObject);
        });
    }

    updateObject(objType, query, object, callback) {
        mongoose.model(objType).findOneAndUpdate(query, object, {upsert: true}, (err, val) => {
            return callback(err, val);
        });
    }

    readObject(objType, query, callback) {
        mongoose.model(objType).find(query, function(err, val) {
            return callback(err, val);
        });
    }

    removeObject(objType, query, callback) {
        mongoose.model(objType).remove(query, function(err, val) {
            return callback(err, val);
        });
    }
}

module.exports = MongoStorageProvider;
